package com.JPA.TestBed;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Student")
public class Student 
{
	@Id
    private int rollNo;
    private String name;
    private int marks;
    private int rank;
    
    Student()
    {
    	this.name = "placeholder";
    	this.rollNo = 0;
    	this.marks = 0;
    	this.rank = 0;
    }
    
    Student(String name, int roll, int marks, int rank)
    {
    	this.name = name;
    	this.rollNo = roll;
    	this.marks = marks;
    	this.rank = rank;
    }

	/**
	 * @return the marks
	 */
	public int getMarks() {
		return marks;
	}
	/**
	 * @param marks the marks to set
	 */
	public void setMarks(int marks) {
		this.marks = marks;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the rollNo
	 */
	public int getRollNo() {
		return rollNo;
	}
	/**
	 * @param rollNo the rollNo to set
	 */
	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}

	/**
	 * @return the rank
	 */
	public int getRank() {
		return rank;
	}

	/**
	 * @param rank the rank to set
	 */
	public void setRank(int rank) {
		this.rank = rank;
	}
}
